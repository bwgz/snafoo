package com.nerdery.nat.snafoo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Vote {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    private Date created;
    private String userId;
    private long snackId;
    
    public long getId() {
        return id;
    }

	public long getSnackId() {
		return snackId;
	}

	public void setSnackId(long snackId) {
		this.snackId = snackId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
    @Override
    public String toString() {
        return String.format("Vote[id=%d, created=%s, userId=%s, snackId=%d]", id, created, userId, snackId);
    }

}
