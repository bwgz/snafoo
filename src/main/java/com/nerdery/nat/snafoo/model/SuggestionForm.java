package com.nerdery.nat.snafoo.model;

public class SuggestionForm {
    private String snackId;
    private String name;
    private String location;
    
	public String getSnackId() {
		return snackId;
	}

	public void setSnackId(String snackId) {
		this.snackId = snackId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
    public String toString() {
        return String.format("SuggestionForm[snackId=%s, name=%s, location=%s]", snackId, name, location);
    }


}
