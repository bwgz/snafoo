package com.nerdery.nat.snafoo.model;

public class Ballot {
	private Snack snack;
	private long votes;
	private boolean enabled;
	
	public Ballot(Snack snack) {
		this.snack = snack;
	}
	
	public Snack getSnack() {
		return snack;
	}
	public void setSnack(Snack snack) {
		this.snack = snack;
	}
	public long getVotes() {
		return votes;
	}
	public void setVotes(long votes) {
		this.votes = votes;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
    @Override
    public String toString() {
        return String.format("Ballot[snack=%s, votes=%d, enabled=%s]", snack.toString(), votes, enabled);
    }
	
}
