package com.nerdery.nat.snafoo.model;

public class VoteCount {
	private long snackId;
	private long count;

	public VoteCount(long snackId, long count) {
		this.snackId = snackId;
		this.count = count;
	}

	public long getSnackId() {
		return snackId;
	}

	public void setSnackId(long snackId) {
		this.snackId = snackId;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
}
