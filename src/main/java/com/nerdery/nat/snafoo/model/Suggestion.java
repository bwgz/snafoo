package com.nerdery.nat.snafoo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Suggestion {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private Date created;
    private String userId;
	private int snackId;
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getSnackId() {
		return snackId;
	}

	public void setSnackId(int snackId) {
		this.snackId = snackId;
	}

	@Override
    public String toString() {
        return String.format("Suggestion[id=%d, created=%s, userId=%s, snackId=%d]", id, created, userId, snackId);
    }

}
