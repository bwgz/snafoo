package com.nerdery.nat.snafoo.model;

import com.google.api.client.util.Key;

public class PurchaseLocation {
	@Key
	private Integer id;
	@Key
	private String name;
	@Key
	private Double latitude;
	@Key
	private Double longitude;
	@Key
	private int preference;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public int getPreference() {
		return preference;
	}
	public void setPreference(int preference) {
		this.preference = preference;
	}
}
