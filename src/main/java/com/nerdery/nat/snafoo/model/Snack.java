package com.nerdery.nat.snafoo.model;

import javax.annotation.Nullable;

import com.google.api.client.util.Key;
import com.google.common.base.Predicate;

public class Snack {
	@Key
	private int id;
	@Key
	private String name;
	@Key
	private boolean optional;
	@Key
	private PurchaseLocation[] purchaseLocations;
	@Key
	private int purchaseCount;
	@Key
	private String lastPurchaseDate; 

	public static Predicate<Snack> byOptional = new Predicate<Snack>() {
		public boolean apply(Snack snack) {
			return snack.isOptional();
		}
	};

	public static Predicate<Snack> byNotOptional = new Predicate<Snack>() {
		public boolean apply(Snack snack) {
			return !snack.isOptional();
		}
	};

	public static class FilterById implements Predicate<Snack> {
		private int id;
		
		public FilterById(int id) {
			this.id = id;
		}
		
		@Override
		public boolean apply(Snack input) {
			return input.id == id;
		}
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOptional() {
		return optional;
	}
	public void setOptional(boolean optional) {
		this.optional = optional;
	}
	public PurchaseLocation[] getPurchaseLocations() {
		return purchaseLocations;
	}
	public void setPurchaseLocations(PurchaseLocation[] purchaseLocations) {
		this.purchaseLocations = purchaseLocations;
	}
	public int getPurchaseCount() {
		return purchaseCount;
	}
	public void setPurchaseCount(int purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	public String getLastPurchaseDate() {
		return lastPurchaseDate;
	}
	public void setLastPurchaseDate(String lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate;
	}
}
