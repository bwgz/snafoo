package com.nerdery.nat.snafoo.home;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nerdery.nat.snafoo.model.Ballot;
import com.nerdery.nat.snafoo.model.Snack;
import com.nerdery.nat.snafoo.model.Suggestion;
import com.nerdery.nat.snafoo.model.SuggestionForm;
import com.nerdery.nat.snafoo.model.Vote;
import com.nerdery.nat.snafoo.model.VoteCount;
import com.nerdery.nat.snafoo.repository.SuggestionRepository;
import com.nerdery.nat.snafoo.repository.VoteRepository;
import com.nerdery.nat.snafoo.service.SnackService;
import com.nerdery.nat.snafoo.service.SuggestionService;
import com.nerdery.nat.snafoo.service.VoteService;

@Controller
public class HomeController {
    private static Logger logger = LoggerFactory.getLogger(HomeController.class);
    private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    private static int USER_VOTES_MAX		= 3;
    private static int USER_SUGGESTIONS_MAX	= 1;
    
    private static int MAX_SHOPPING_LIST_SNACKS	= 10;
    
    @Autowired
	private SnackService snackService;
    
    @Autowired
	private VoteService voteService;
    
    @Autowired
	private SuggestionService suggestionService;

    @Autowired
    private VoteRepository voteRepository;
    
    @Autowired
    private SuggestionRepository suggestionRepository;

    static final private class DateRange {
    	private Date start;
    	private Date end;
    	
    	static DateRange getThisMonth() throws ParseException {
    		Calendar calendar = new GregorianCalendar();
    		int month = calendar.get(Calendar.MONTH) + 1;
    		int year = calendar.get(Calendar.YEAR);
    		String start = String.format("%02d/01/%d", month, year);
    		String end = String.format("%02d/01/%d", month < 12 ? month + 1 : 1, month < 12 ? year : year + 1);
        	logger.debug(String.format("index - start: %s  end: %s", start, end));
   		
        	return new DateRange(dateFormat.parse(start), dateFormat.parse(end));
    	}
    	
    	public DateRange(Date start, Date end) {
    		this.start = start;
    		this.end = end;
    	}
    	
		public Date getStart() {
			return start;
		}
		
		public void setStart(Date start) {
			this.start = start;
		}
		
		public Date getEnd() {
			return end;
		}
		
		public void setEnd(Date end) {
			this.end = end;
		}
    }

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(HttpServletRequest request, Model model) throws IOException, ParseException {
    	logger.debug(String.format("index - snackService: %s", snackService));
    	logger.debug(String.format("index - repository: %s", voteRepository));
    	logger.debug(String.format("index - model: %s", model));
    	
    	String userId = request.getAttribute("snafoo").toString();
    	logger.debug(String.format("index - snafoo: %s", userId));

		model.addAttribute("today", new Date().toString());
		model.addAttribute("always", snackService.findAlwaysPurchased());
		model.addAttribute("optional", snackService.findOptionalPurchased());
		
		List<Vote> list = voteRepository.findAll();
    	logger.debug(String.format("votes: %d", list.size()));

    	DateRange dateRange = DateRange.getThisMonth();
    	
    	list = voteRepository.findByUserAndDateRange(userId, dateRange.getStart(), dateRange.getEnd());
    	logger.debug(String.format("user's votes: %d", list.size()));
    	
    	int votesLeft = Math.max(USER_VOTES_MAX - list.size(), 0);
    	logger.debug(String.format("user's votes left: %d", votesLeft));
    	
		model.addAttribute("votesLeft", votesLeft);
		model.addAttribute("canVote", votesLeft != 0);

    	List<Ballot> ballots = new ArrayList<Ballot>();
		for (Suggestion suggestion : suggestionRepository.findByDateRange(dateRange.getStart(), dateRange.getEnd())) {
			Snack snack = snackService.findById(suggestion.getSnackId());
			
			list = voteRepository.findBySnackAndDateRange(snack.getId(), dateRange.getStart(), dateRange.getEnd());
	    	logger.debug(String.format("all within date range - votes: %d", list.size()));
			
			list = voteRepository.findByUserAndSnackAndDateRange(userId, snack.getId(), dateRange.getStart(), dateRange.getEnd());
	    	logger.debug(String.format("user's within date range - votes: %d", list.size()));
			
			Ballot ballot = new Ballot(snack);
			ballot.setVotes(voteRepository.findBySnackAndDateRange(snack.getId(), dateRange.getStart(), dateRange.getEnd()).size());
			ballot.setEnabled(voteRepository.findByUserAndSnackAndDateRange(userId, snack.getId(), dateRange.getStart(), dateRange.getEnd()).size() == 0);
	    	logger.debug(String.format("index - ballot: %s", ballot));
			ballots.add(ballot);
		}
		
		model.addAttribute("ballots", ballots);
		
		return "home/index";
	}

    @RequestMapping(value="/vote")
    public String vote(@RequestParam("snackId") long snackId, HttpServletRequest request, Model model) throws IOException, ParseException {
    	logger.debug(String.format("vote - repository: %s", voteRepository));
    	logger.debug(String.format("vote - snackId: %d  model: %s", snackId, model));
    	
    	Vote vote = new Vote();
    	vote.setCreated(new Date());
    	vote.setSnackId(snackId);
    	vote.setUserId(request.getAttribute("snafoo").toString());
    	voteRepository.save(vote);
    	logger.debug(String.format("vote - vote: %s", vote));
    	
    	return index(request, model);
    }

	@RequestMapping(value = "/suggestions", method = RequestMethod.GET)
	public String suggestions(HttpServletRequest request, Model model) throws IOException, ParseException {
	    logger.debug(String.format("suggestions(get) - model: %s", model));
    	String userId = request.getAttribute("snafoo").toString();
    	logger.debug(String.format("index - snafoo: %s", userId));
	    
    	DateRange dateRange = DateRange.getThisMonth();
    	
    	List<Suggestion> list = suggestionRepository.findByUserAndDateRange(userId, dateRange.getStart(), dateRange.getEnd());
    	logger.debug(String.format("user's suggestions: %d", list.size()));
    	
    	int suggestionsLeft = Math.max(USER_SUGGESTIONS_MAX - list.size(), 0);
    	logger.debug(String.format("user's suggestions left: %d", suggestionsLeft));
    	
		model.addAttribute("canSuggest", suggestionsLeft != 0);
    	
		List<Snack> snacks = new ArrayList<Snack>();
	    
	    for (Snack snack : snackService.findOptionalPurchased()) {
	    	if (suggestionService.findBySnackIdAndDateRange(snack.getId(), dateRange.getStart(), dateRange.getEnd()).size() == 0) {
	    		snacks.add(snack);
	    	}
	    }
	    
		model.addAttribute("snacks", snacks);
        model.addAttribute("suggestionForm", new SuggestionForm());
		return "home/suggestions";
	}
	
	@RequestMapping(value = "/suggestions", method = RequestMethod.POST)
	public String suggestions(@ModelAttribute SuggestionForm suggestionForm, HttpServletRequest request, Model model) throws IOException, ParseException, GeneralSecurityException {
	    logger.debug(String.format("suggestions(post) - suggestionForm: %s", suggestionForm));
	    logger.debug(String.format("suggestions(post) - model: %s", model));
    	String userId = request.getAttribute("snafoo").toString();
    	logger.debug(String.format("index - snafoo: %s", userId));
	    
    	DateRange dateRange = DateRange.getThisMonth();
    	
    	List<Suggestion> list = suggestionRepository.findByUserAndDateRange(userId, dateRange.getStart(), dateRange.getEnd());
    	logger.debug(String.format("user's suggestions: %d", list.size()));
    	
    	int suggestionsLeft = Math.max(USER_SUGGESTIONS_MAX - list.size(), 0);
    	logger.debug(String.format("user's suggestions left: %d", suggestionsLeft));
    	
		if (suggestionsLeft != 0) {
			Suggestion suggestion;
			
			int snackId = Integer.parseInt(suggestionForm.getSnackId());
	    	logger.debug(String.format("snackId: %d", snackId));
			if (snackId != -1) {
				Snack snack = snackService.findById(snackId);
			    logger.debug(String.format("suggestion from snack: %s", snack));
				
				suggestion = new Suggestion();
		    	suggestion.setCreated(new Date());
		    	suggestion.setUserId(userId);
		    	suggestion.setSnackId(snack.getId());
			}
			else {
		    	Snack snack = suggestionService.post(suggestionForm.getName(), suggestionForm.getLocation());
				suggestion = new Suggestion();
		    	suggestion.setCreated(new Date());
		    	suggestion.setUserId(userId);
		    	suggestion.setSnackId(snack.getId());
			}
			
		    suggestionRepository.save(suggestion);
		    logger.debug(String.format("suggestion: %s", suggestion));
		}

    	list = suggestionRepository.findByUserAndDateRange(userId, dateRange.getStart(), dateRange.getEnd());
    	logger.debug(String.format("user's suggestions: %d", list.size()));
    	
    	suggestionsLeft = Math.max(USER_SUGGESTIONS_MAX - list.size(), 0);
    	logger.debug(String.format("user's suggestions left: %d", suggestionsLeft));
    	
		model.addAttribute("canSuggest", suggestionsLeft != 0);

		return "home/suggestions";
	}
	
	@RequestMapping(value = "/shoppinglist", method = RequestMethod.GET)
	public String shoppinglist(HttpServletRequest request, Model model) throws IOException, ParseException {
    	logger.debug(String.format("shoppinglist - snackService: %s", snackService));
    	logger.debug(String.format("shoppinglist - repository: %s", voteRepository));
    	logger.debug(String.format("shoppinglist - model: %s", model));
    	
    	Collection<Snack> always = snackService.findAlwaysPurchased();
    	List<Snack> list = new ArrayList<Snack>(always);
		
		int limit = Math.max(MAX_SHOPPING_LIST_SNACKS - list.size(), 0);
    	logger.debug(String.format("shoppinglist - limit: %d", limit));
		
		if (limit > 0) {
	    	DateRange dateRange = DateRange.getThisMonth();
			
			List<VoteCount> counts = voteRepository.findVoteCountByDateRange(dateRange.getStart(), dateRange.getEnd());
			for (int i = 0; i < counts.size() && i < limit; i++) {
				VoteCount count = counts.get(i);
		    	logger.debug(String.format("shoppinglist - snackId: %d  votes: %d", count.getSnackId(), count.getCount()));
		    	
				Snack snack = snackService.findById((int) count.getSnackId());
				list.add(snack);
			}
		}
		
		Collections.sort(list, new Comparator<Snack>() {
			@Override
			public int compare(Snack arg0, Snack arg1) {
				int result;
				
				if (arg0.getPurchaseLocations().length != 0 && arg1.getPurchaseLocations().length == 0) {
					result = -1;
				}
				else if (arg0.getPurchaseLocations().length == 0 && arg1.getPurchaseLocations().length == 0) {
					result = 0;
				}
				else if (arg0.getPurchaseLocations().length == 0 && arg1.getPurchaseLocations().length != 0) {
					result = 1;
				}
				else {
					result = arg0.getPurchaseLocations()[0].getName().compareTo(arg1.getPurchaseLocations()[0].getName());
				}
				
				return result;
			}
		});
		
		model.addAttribute("list", list);
		
		return "home/shoppinglist";
	}
	
    @RequestMapping("properties")
    @ResponseBody
    Properties properties() {
        return System.getProperties();
    }

}
