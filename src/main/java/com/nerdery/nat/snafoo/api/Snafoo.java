package com.nerdery.nat.snafoo.api;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpMethods;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.nerdery.nat.snafoo.model.Snack;

public class Snafoo extends AbstractGoogleJsonClient {
	public static final String DEFAULT_ROOT_URL = "https://api-snacks-staging.nerderylabs.com/";
	public static final String DEFAULT_SERVICE_PATH = "v1/";
	public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

	public Snafoo(HttpTransport transport, JsonFactory jsonFactory) {
		this(new Builder(transport, jsonFactory));
	}

	protected Snafoo(Builder builder) {
		super(builder);
	}

	@Override
	protected void initialize(AbstractGoogleClientRequest<?> httpClientRequest) throws java.io.IOException {
		super.initialize(httpClientRequest);
	}

	public Snacks snacks() throws java.io.IOException {
		Snacks result = new Snacks();
		initialize(result);
		return result;
	}

	public class Snacks extends SnafooRequest<SnacksResponse> {
	    private static final String REST_PATH = "snacks";

	    public Snacks() {
		    super(Snafoo.this, "GET", REST_PATH, null, SnacksResponse.class);
		}
	}

	public Suggestion suggestion() throws IOException, GeneralSecurityException {
		Suggestion result = new Suggestion();
		return result;
	}

	public class Suggestion {
		public static final String SNACKS_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH + "snacks";
		public final GenericUrl SNACKS_GENERIC_URL = new GenericUrl(SNACKS_BASE_URL + "?ApiKey=f6b615eb-b49c-4928-9787-0e125d94bf88");
		private NetHttpTransport httpTransport;

	    public Suggestion() throws GeneralSecurityException, IOException {
	        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		}
	    
		public Snack execute(String name, String location) throws IOException {
	    	Map<String, Object> json = new HashMap<String, Object>();
	    	
	    	long value = System.currentTimeMillis();
	    	json.put("Name", name);
	    	json.put("Location", location);

	    	HttpContent content = new JsonHttpContent(new JacksonFactory(), json);
	    	HttpRequest request = httpTransport.createRequestFactory().buildPostRequest(SNACKS_GENERIC_URL, content);

	        request.setRequestMethod(HttpMethods.POST);
	        HttpResponse response = request.execute();
	        
	        ObjectMapper mapper = new ObjectMapper();
	        return mapper.readValue(response.getContent(), Snack.class);
	    }
	}

	public static final class Builder extends AbstractGoogleJsonClient.Builder {

		public Builder(HttpTransport httpTransport, JsonFactory jsonFactory) {
			super(httpTransport, jsonFactory, DEFAULT_ROOT_URL, DEFAULT_SERVICE_PATH, null, false);
		}

	    @Override
	    public Builder setApplicationName(String applicationName) {
	      return (Builder) super.setApplicationName(applicationName);
	    }

		public Snafoo build() {
			return new Snafoo(this);
		}

	}

}
