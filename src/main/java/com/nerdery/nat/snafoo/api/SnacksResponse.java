package com.nerdery.nat.snafoo.api;

import java.util.ArrayList;

import com.nerdery.nat.snafoo.model.Snack;

public class SnacksResponse extends ArrayList<Snack> {
	private static final long serialVersionUID = -8118973355003577315L;
}
