package com.nerdery.nat.snafoo.api;

import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Key;

public abstract class SnafooRequest<T> extends AbstractGoogleJsonClientRequest<T> {
	public SnafooRequest(Snafoo client, String method, String uriTemplate, Object content, Class<T> responseClass) {
		super(client, method, uriTemplate, content, responseClass);
	}

	@Key("ApiKey")
	private String apiKey;

	public String getApiKey() {
		return apiKey;
	}

	public SnafooRequest<T> setApiKey(String apiKey) {
		this.apiKey = apiKey;
		return this;
	}

	@Override
	public final Snafoo getAbstractGoogleClient() {
		return (Snafoo) super.getAbstractGoogleClient();
	}

	@Override
	public SnafooRequest<T> setDisableGZipContent(boolean disableGZipContent) {
		return (SnafooRequest<T>) super.setDisableGZipContent(disableGZipContent);
	}

	@Override
	public SnafooRequest<T> setRequestHeaders(HttpHeaders headers) {
		return (SnafooRequest<T>) super.setRequestHeaders(headers);
	}

	@Override
	public SnafooRequest<T> set(String parameterName, Object value) {
		return (SnafooRequest<T>) super.set(parameterName, value);
	}
}