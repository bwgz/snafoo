package com.nerdery.nat.snafoo.api;

import com.google.api.client.util.Key;

public class SuggestionRequest<T> extends SnafooRequest<T> {

	public SuggestionRequest(Snafoo client, String method, String uriTemplate, Object content, Class<T> responseClass) {
		super(client, method, uriTemplate, content, responseClass);
	}

	@Key("name")
	private String name;

	public String getName() {
		return name;
	}

	public SuggestionRequest<T> setName(String name) {
		this.name = name;
		return this;
	}


}
