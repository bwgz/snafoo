package com.nerdery.nat.snafoo.service;

import java.io.IOException;
import java.util.Collection;

import com.nerdery.nat.snafoo.model.Snack;

public interface SnackService {
	Snack findById(int id) throws IOException;
	Collection<Snack> findAll() throws IOException;
    Collection<Snack> findAlwaysPurchased() throws IOException;
    Collection<Snack> findOptionalPurchased() throws IOException;
}
