package com.nerdery.nat.snafoo.service;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.nerdery.nat.snafoo.model.Vote;
import com.nerdery.nat.snafoo.repository.VoteRepository;

@Service
@Validated
public class VoteServiceImpl implements VoteService {
    private final VoteRepository repository;

    @Inject
    public VoteServiceImpl(final VoteRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
	public Vote save(@NotNull @Valid final Vote snack) {
        return repository.save(snack);
	}

    @Override
    @Transactional(readOnly = true)
	public List<Vote> getList() {
        return repository.findAll();
	}

}
