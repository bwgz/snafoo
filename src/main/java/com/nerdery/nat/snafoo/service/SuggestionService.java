package com.nerdery.nat.snafoo.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.nerdery.nat.snafoo.model.Snack;
import com.nerdery.nat.snafoo.model.Suggestion;

public interface SuggestionService {
	public Collection<Suggestion> findAll() throws IOException;
	List<Suggestion> findByDateRange(Date start, Date end) throws IOException;
	List<Suggestion> findBySnackIdAndDateRange(int snackId, Date start, Date end) throws IOException;
	
	public Snack post(String name, String location) throws IOException, GeneralSecurityException;
}
