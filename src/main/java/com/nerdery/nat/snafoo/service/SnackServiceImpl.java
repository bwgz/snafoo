package com.nerdery.nat.snafoo.service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.nerdery.nat.snafoo.model.Snack;

@Service
@Validated
public class SnackServiceImpl extends SnafooService implements SnackService {
    private static Logger logger = LoggerFactory.getLogger(SnackServiceImpl.class);

    @Override
	public List<Snack> findAll() throws IOException {
    	logger.debug("entering findAll");
        return getSnafoo().snacks().setApiKey(getApiKey()).execute();
	}
    
    @Override
	public Collection<Snack> findAlwaysPurchased() throws IOException {
    	logger.debug("entering findAlwaysPurchased");
    	
        return Collections2.filter(getSnafoo().snacks().setApiKey(getApiKey()).execute(), Snack.byNotOptional);
	}
    
    @Override
	public Collection<Snack> findOptionalPurchased() throws IOException {
    	logger.debug("entering findAlwaysPurchased");
    	
        return Collections2.filter(getSnafoo().snacks().setApiKey(getApiKey()).execute(), Snack.byOptional);
	}

	@Override
	public Snack findById(int id) throws IOException {
		return Iterables.find(getSnafoo().snacks().setApiKey(getApiKey()).execute(), new Snack.FilterById(id));  
	}

}
