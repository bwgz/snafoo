package com.nerdery.nat.snafoo.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.nerdery.nat.snafoo.model.Snack;
import com.nerdery.nat.snafoo.model.Suggestion;
import com.nerdery.nat.snafoo.repository.SuggestionRepository;

@Service
@Validated
public class SuggestionServiceImpl extends SnafooService implements SuggestionService {
    private static Logger logger = LoggerFactory.getLogger(SuggestionServiceImpl.class);
    private final SuggestionRepository repository;

    @Inject
    public SuggestionServiceImpl(final SuggestionRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
	public Collection<Suggestion> findAll() throws IOException {
        return repository.findAll();
	}

	@Override
	public List<Suggestion> findByDateRange(Date start, Date end) throws IOException {
    	logger.debug(String.format("findByDateRange - start: %s  end: %s", start, end));
		return repository.findByDateRange(start, end);
	}

	@Override
	public List<Suggestion> findBySnackIdAndDateRange(int snackId, Date start, Date end) throws IOException {
    	logger.debug(String.format("findByNameAndDateRange - name: %d  start: %s  end: %s", snackId, start, end));
		return repository.findBySnackIdAndDateRange(snackId, start, end);
	}

	public Snack post(String name, String location) throws IOException, GeneralSecurityException {
		return getSnafoo().suggestion().execute(name, location);
	}
}
