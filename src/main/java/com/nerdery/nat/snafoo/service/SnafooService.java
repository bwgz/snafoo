package com.nerdery.nat.snafoo.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.nerdery.nat.snafoo.api.Snafoo;

abstract public class SnafooService {
	private String API_KEY = "f6b615eb-b49c-4928-9787-0e125d94bf88";
	
	@Autowired
	private Snafoo snafoo;
	
	protected String getApiKey() {
		return API_KEY;
	}

	protected Snafoo getSnafoo() {
		return snafoo;
	}

	protected void setSnafoo(Snafoo snafoo) {
		this.snafoo = snafoo;
	}
}
