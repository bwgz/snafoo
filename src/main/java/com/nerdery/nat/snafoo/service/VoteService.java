package com.nerdery.nat.snafoo.service;

import java.util.List;

import com.nerdery.nat.snafoo.model.Vote;

public interface VoteService {
    Vote save(Vote vote);

    List<Vote> getList();

}
