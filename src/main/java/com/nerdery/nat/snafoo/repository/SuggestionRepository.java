package com.nerdery.nat.snafoo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nerdery.nat.snafoo.model.Suggestion;

public interface SuggestionRepository extends JpaRepository<Suggestion, Long> {

    @Query("SELECT s FROM Suggestion s WHERE s.created >= :start AND s.created < :end")
    public List<Suggestion> findByDateRange(@Param("start") Date start, @Param("end") Date end);

    @Query("SELECT s FROM Suggestion s WHERE s.snackId = :snackId AND s.created >= :start AND s.created < :end")
    public List<Suggestion> findBySnackIdAndDateRange(@Param("snackId") int snackId, @Param("start") Date start, @Param("end") Date end);

    @Query("SELECT s FROM Suggestion s WHERE s.userId = :userId AND s.created >= :start AND s.created < :end")
    public List<Suggestion> findByUserAndDateRange(@Param("userId") String object, @Param("start") Date start, @Param("end") Date end);
}
