package com.nerdery.nat.snafoo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nerdery.nat.snafoo.model.Vote;
import com.nerdery.nat.snafoo.model.VoteCount;

public interface  VoteRepository extends JpaRepository<Vote, Long> {

    List<Vote> findBySnackId(long snackId);
    
    @Query("SELECT v FROM Vote v WHERE v.created >= :start AND v.created < :end")
    public List<Vote> findByDateRange(@Param("start") Date start, @Param("end") Date end);
    
    @Query("SELECT new com.nerdery.nat.snafoo.model.VoteCount(v.snackId, count(v) as votes) FROM Vote v GROUP BY v.snackId ORDER BY votes DESC")
    public List<VoteCount> findVoteCount();
    
    @Query("SELECT new com.nerdery.nat.snafoo.model.VoteCount(v.snackId, count(v) as votes) FROM Vote v WHERE v.created >= :start AND v.created < :end GROUP BY v.snackId ORDER BY votes DESC")
    public List<VoteCount> findVoteCountByDateRange(@Param("start") Date start, @Param("end") Date end);
	
    @Query("SELECT v FROM Vote v WHERE v.snackId = :snackId AND v.created >= :start AND v.created < :end")
    public List<Vote> findBySnackAndDateRange(@Param("snackId") long snackId, @Param("start") Date start, @Param("end") Date end);

    @Query("SELECT v FROM Vote v WHERE v.userId = :userId AND v.created >= :start AND v.created < :end")
    public List<Vote> findByUserAndDateRange(@Param("userId") String object, @Param("start") Date start, @Param("end") Date end);
    
    @Query("SELECT v FROM Vote v WHERE v.userId = :userId AND v.snackId = :snackId AND v.created >= :start AND v.created < :end")
    public List<Vote> findByUserAndSnackAndDateRange(@Param("userId") String object, @Param("snackId") long snackId, @Param("start") Date start, @Param("end") Date end);

}
