package com.nerdery.nat.snafoo.config;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.nerdery.nat.snafoo.Application;
import com.nerdery.nat.snafoo.api.Snafoo;

@Configuration
@ComponentScan(basePackageClasses = Application.class, excludeFilters = @Filter({Controller.class, Configuration.class}))
class ApplicationConfig {

    private HttpTransport httpTransport() throws GeneralSecurityException, IOException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }
    
    private JsonFactory jsonFactory() {
        return JacksonFactory.getDefaultInstance();
    }
    
    @Bean
    public Snafoo snafoo() throws GeneralSecurityException, IOException {
        return new Snafoo(httpTransport(), jsonFactory());
    }
    
}