package com.nerdery.nat.snafoo.config;

import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class ModelCookieInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(ModelCookieInterceptor.class);
    
    private static String COOKIE_NAME	= "snafoo";
    
    private Cookie getSnafooCookie(Cookie[] cookies) {
    	Cookie result = null;
    	
    	if (cookies != null) {
	    	for (Cookie cookie : cookies) {
	            if (cookie.getName().equals(COOKIE_NAME)) {
	            	result = cookie;
	            	break;
	            }
	    	}
    	}
    	
    	return result;
    }

    private Cookie getSnafooCookie(HttpServletRequest request) {
    	return getSnafooCookie(request.getCookies());
    }
    
    /*
     * This application establishes the user's context based on a cookie. It intercepts
     * the request. If a cookie exists it reads the user's unique id otherwise it creates one. 
     */
    private void establishUserContext(HttpServletRequest request, HttpServletResponse response) {
    	Cookie cookie = getSnafooCookie(request);
    	if (cookie == null) {
    		cookie = new Cookie(COOKIE_NAME, UUID.randomUUID().toString());
    		cookie.setMaxAge(Integer.MAX_VALUE);
    		response.addCookie(cookie);
    	}
    	logger.debug(String.format("cookie: %s", cookie));
    	
    	String uuid = cookie.getValue();
    	logger.debug(String.format("uuid: %s", uuid));
	
    	request.setAttribute("snafoo", uuid);
    }
    
    @Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	logger.debug(String.format("preHandle - request: %s  response: %s", request, response));
    	
    	establishUserContext(request, response);
    	
		return true;
	}
}
