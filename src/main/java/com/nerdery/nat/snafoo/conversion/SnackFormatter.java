package com.nerdery.nat.snafoo.conversion;

import java.io.IOException;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import com.nerdery.nat.snafoo.model.Snack;
import com.nerdery.nat.snafoo.service.SnackService;

public class SnackFormatter implements Formatter<Snack> {

    @Autowired
    private SnackService snackService;

    public SnackFormatter() {
        super();
    }

    public Snack parse(final String text, final Locale locale) throws ParseException {
    	Snack snack = null;
    	
        final Integer id = Integer.valueOf(text);
        try {
			snack = snackService.findById(id);
		} catch (IOException e) {
		}
        
        return snack;
    }

    public String print(final Snack snack, final Locale locale) {
        return (snack != null ? String.valueOf(snack.getId()) : "");
    }

}
