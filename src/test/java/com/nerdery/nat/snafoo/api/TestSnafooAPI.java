package com.nerdery.nat.snafoo.api;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.nerdery.nat.snafoo.api.Snafoo.Suggestion;
import com.nerdery.nat.snafoo.model.Snack;

public class TestSnafooAPI {
	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static Snafoo snafoo;
	
    @Before
    public void before() throws GeneralSecurityException, IOException {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        snafoo = new Snafoo.Builder(httpTransport, JSON_FACTORY).setApplicationName(TestSnafooAPI.class.getName()).build();
    }

    @Test
    public void good() {
    	SnacksResponse json;
		try {
			json = snafoo.snacks().setApiKey("f6b615eb-b49c-4928-9787-0e125d94bf88").execute();
			for (Snack snack : json) {
				System.out.printf("name: %s (%d)  optional: %s  lastPurchaseDate: %s\n", snack.getName(), snack.getId(), snack.isOptional(), snack.getLastPurchaseDate());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    //@Test
    public void bad() {
    	SnacksResponse json;
		try {
			json = snafoo.snacks().setApiKey("a-really-bad-key").execute();
			System.out.println(json.toString());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
    }
    
    //@Test
    /*
    public void suggestion() {
    	String json = "{\"name\": \"Super Snack\",\"location\": \"John's Grocery\"}";

		try {
	    	Map<String, Object> properties = new HashMap<String, Object>();
	    	
	    	long value = System.currentTimeMillis();
	    	properties.put("Name", "Test Snack " + value);
	    	properties.put("Location", "Test Location " + value);
	    	properties.put("Latitude", 44.8587646057615);
	    	properties.put("Longitude", -93.3331243773672);
	    	HttpContent content = new JsonHttpContent(new JacksonFactory(), properties);
	    	
			Suggestion suggestion = snafoo.suggestion(content.toString());
			System.out.printf("content: %s\n", content.toString());
			
			suggestion.setApiKey("f6b615eb-b49c-4928-9787-0e125d94bf88");
			
			SnacksResponse response = suggestion.execute();
			System.out.println(properties.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    */

}
