package com.nerdery.nat.snafoo.api;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpMethods;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.testing.http.HttpTesting;

public class PostTest {
	public static final String DEFAULT_ROOT_URL = "https://api-snacks-staging.nerderylabs.com/";
	public static final String DEFAULT_SERVICE_PATH = "v1/";
	public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;
	public static final String SNACKS_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH + "snacks";

	public static final GenericUrl SNACKS_GENERIC_URL = new GenericUrl(SNACKS_BASE_URL + "?ApiKey=f6b615eb-b49c-4928-9787-0e125d94bf88");

	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	
    @Before
    public void before() throws GeneralSecurityException, IOException {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    }

    @Test
    public void good() throws IOException {
    	
    	Map<String, Object> json = new HashMap<String, Object>();
    	
    	long value = System.currentTimeMillis();
    	json.put("Name", "Test Snack " + value);
    	json.put("Location", "Test Location " + value);
    	json.put("Latitude", 44.8587646057615);
    	json.put("Longitude", -93.3331243773672);
    	HttpContent content = new JsonHttpContent(new JacksonFactory(), json);
    	HttpRequest request = httpTransport.createRequestFactory().buildPostRequest(SNACKS_GENERIC_URL, content);

        request.setRequestMethod(HttpMethods.POST);
        HttpResponse response = request.execute();

        System.out.printf("response: %s\n", response);
        Assert.assertEquals(200, response.getStatusCode());
        Assert.assertNotNull(request.getContent());
    }
}
